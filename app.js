const control = document.querySelectorAll('.control')
const input = document.querySelector('#tempo-control')
const tempoDisplay = document.querySelector('.tempo-display')
const play = document.querySelector('.play')
const audio = document.querySelector('.audio')
const btn = document.querySelectorAll('.btn')

const appState = {
    tempoSpeed: 500,
    playing: false
}

const getMilliseconds = (time) => {
    return  Math.floor((60 / parseInt(time)) * 1000)
}

const changeTempo = (e) => {
    if(appState.playing) {
        return startAndRestart()
    }

    let tempo = input.value
    if(e.target.textContent === '-') {
        tempo--
    } else {
        tempo++    
    }
    input.value = tempo
    appState.tempoSpeed = getMilliseconds(input.value)
    tempoDisplay.textContent = input.value + ' BPM'
}

const startMetronone = () => {
    let counter = 0
    appState.playing = !appState.playing
    input.disabled = !input.disabled
    let start = setInterval(() => {

        if(counter === 4) {
            counter = 0
        }
        if(appState.playing === false) {
            console.log('Stop')
            counter = 0  
            clearInterval(start)
        } else {
            audio.play()
            play.classList.add('playing')
            btn[counter].classList.add('add-blue')  
            console.log('Tick')
            setTimeout(() => {
                play.classList.remove('playing') 
                btn[counter].classList.remove('add-blue') 
                counter++
            },100)
        }
    }, appState.tempoSpeed)
}

const startAndRestart = () => {
    clearInterval(startMetronone)
}

play.addEventListener('click', startMetronone)

control.forEach(element => {
        element.addEventListener('click', changeTempo)
});

input.addEventListener('input', changeTempo)

window.addEventListener('load', () => {
    tempoDisplay.textContent = input.value + ' BPM'
})